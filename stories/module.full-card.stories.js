import React from "react";

import { storiesOf } from "@storybook/react";

import { FullCard } from "../src/components";
import { FullCardModule } from "../src/modules";

const images = [
  "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg"
];

const list = [
  {
    title: "Apartment",
    subTitle: "(34.555 Homes)",
    image: images[0],
    url: "https://goveta.co"
  },
  {
    title: "Apartment",
    subTitle: "(34.555 Homes)",
    image: images[0],
    url: "https://goveta.co"
  },
  {
    title: "Apartment",
    subTitle: "(34.555 Homes)",
    image: images[0],
    url: "https://goveta.co"
  },
  {
    title: "Apartment",
    subTitle: "(34.555 Homes)",
    image: images[0],
    url: "https://goveta.co"
  },
  {
    title: "Apartment",
    subTitle: "(34.555 Homes)",
    image: images[0],
    url: "https://goveta.co"
  },
  {
    title: "Apartment",
    subTitle: "(34.555 Homes)",
    image: images[0],
    url: "https://goveta.co"
  },
  {
    title: "Apartment",
    subTitle: "(34.555 Homes)",
    image: images[0],
    url: "https://goveta.co"
  },
  {
    title: "Apartment",
    subTitle: "(34.555 Homes)",
    image: images[0],
    url: "https://goveta.co"
  }
];

storiesOf("Modules/FullCard", module).add("Default", () => (
  <FullCardModule title="What can we help you find?" subTitle="All countries">
    {list.map((item, index) => (
      <FullCard key={index} {...item} />
    ))}
  </FullCardModule>
));
