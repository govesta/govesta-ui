import React from "react";

import { storiesOf } from "@storybook/react";

import { Autocomplete } from "../src/components";

async function wait(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}

storiesOf("Components/Autocomplete", module)
  .add("Default", () => (
    <Autocomplete
      placeholder="Kreuzberg, Berlin"
      icon={"icon-search"}
      optionIcon={"icon-facebook"}
      onSelect={value => {
        console.log(value);
      }}
      onLoad={async () => {
        await wait(1000);
        return [
          { value: "chocolate", label: "Chocolate" },
          { value: "strawberry", label: "Strawberry" },
          { value: "vanilla", label: "Vanilla" }
        ];
      }}
    />
  ))
  .add("Small", () => (
    <Autocomplete
      size="small"
      icon={"icon-search"}
      placeholder="Kreuzberg, Berlin"
      onSelect={value => {
        console.log(value);
      }}
      onLoad={async () => {
        await wait(1000);
        return [
          { value: "chocolate", label: "Chocolate" },
          { value: "strawberry", label: "Strawberry" },
          { value: "vanilla", label: "Vanilla" }
        ];
      }}
    />
  ))
  .add("No Shadow", () => (
    <Autocomplete
      shadow={false}
      size="small"
      placeholder="Kreuzberg, Berlin"
      onSelect={value => {
        console.log(value);
      }}
      onLoad={async () => {
        await wait(1000);
        return [
          { value: "chocolate", label: "Chocolate" },
          { value: "strawberry", label: "Strawberry" },
          { value: "vanilla", label: "Vanilla" }
        ];
      }}
    />
  ));
