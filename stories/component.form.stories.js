import React from "react";

import { storiesOf } from "@storybook/react";

import { UIForm } from "../src/components";

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' }
]

storiesOf("Components/Form", module)
  .add("Input", () => (
    <UIForm.Field label="First Name">
      <UIForm.Input placeholder="Your Add Here..." />
    </UIForm.Field>
  )).add("Input Numeric", () => (
    <UIForm.Field label="First Name">
      <UIForm.Input numeric value={11} placeholder="Your Add Here..." suffix="SQM" onChange={(value) => { console.log(value) }} />
    </UIForm.Field>
  )).add("Input with Suffix", () => (
    <UIForm.Field label="First Name">
      <UIForm.Input placeholder="Your Add Here..." suffix="SQM" />
    </UIForm.Field>
  )).add("Input Error", () => (
    <UIForm.Field label="First Name" error="*Required">
      <UIForm.Input placeholder="Your Add Here..." suffix="SQM" />
    </UIForm.Field>
  )).add("TextArea", () => (
    <UIForm.Field label="Description">
      <UIForm.TextArea placeholder="Your Description Here..." />
    </UIForm.Field>
  )).add("TextArea Error", () => (
    <UIForm.Field label="Description" error="*Required">
      <UIForm.TextArea placeholder="Your Description Here..." />
    </UIForm.Field>
  )).add("Select", () => (
    <UIForm.Field label="Options">
      <UIForm.Select value="vanilla" placeholder="Select a Option" options={options} onChange={(value) => { console.log(value) }} />
    </UIForm.Field>
  )).add("Select Error", () => (
    <UIForm.Field label="Options" error="*Required">
      <UIForm.Select value="vanilla" placeholder="Select a Option" options={options} onChange={(value) => { console.log(value) }} />
    </UIForm.Field>
  )).add("CheckBox", () => (
    <UIForm.CheckBox>
      <span>Price on Request</span>
    </UIForm.CheckBox>
  ))