import React from "react";

import { storiesOf } from "@storybook/react";

import { Button } from "../src/components";

storiesOf("Components/Button", module)
  .add("Brand Border", () => <Button theme="brand-border">Kreuzberg</Button>)
  .add("Loadng", () => <Button loading>Kreuzberg</Button>)
  .add("Brand", () => <Button theme="brand">Kreuzberg</Button>)
  .add("Brand Dark", () => <Button theme="brand-dark">Kreuzberg</Button>)
  .add("Brand Light", () => <Button theme="brand-light">Kreuzberg</Button>)
  .add("Brand Blue", () => <Button theme="blue">Kreuzberg</Button>)
  .add("Medium", () => (
    <Button theme="brand" size="medium">
      Kreuzberg
    </Button>
  ));
