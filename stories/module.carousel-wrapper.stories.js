import React from "react";

import { storiesOf } from "@storybook/react";

import { DistrictCard, PopularCountryCard, PopularCityCard, PropertyDetailsCard } from "../src/components";

import { DistrictsModule, PopularCityModule, PopularCountryModule, PropertyDetailsModule } from "../src/modules";

const images = [
  "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg"
];

const list = [
  {
    title: "Apartment",
    subTitle: "(34.555 Homes)",
    image: images[0],
    url: "https://goveta.co"
  },
  {
    title: "Apartment",
    subTitle: "(34.555 Homes)",
    image: images[0],
    url: "https://goveta.co"
  },
  {
    title: "Apartment",
    subTitle: "(34.555 Homes)",
    image: images[0],
    url: "https://goveta.co"
  },
  {
    title: "Apartment",
    subTitle: "(34.555 Homes)",
    image: images[0],
    url: "https://goveta.co"
  },
  {
    title: "Apartment",
    subTitle: "(34.555 Homes)",
    image: images[0],
    url: "https://goveta.co"
  },
  {
    title: "Apartment",
    subTitle: "(34.555 Homes)",
    image: images[0],
    url: "https://goveta.co"
  },
  {
    title: "Apartment",
    subTitle: "(34.555 Homes)",
    image: images[0],
    url: "https://goveta.co"
  },
  {
    title: "Apartment",
    subTitle: "(34.555 Homes)",
    image: images[0],
    url: "https://goveta.co"
  }
];

const propertyDetailsList = [
  {
    title: "Berlin, Mitte · Penthouse · Center",
    location: "Berlin, Kreuzberg",
    features: [{title: "4"}, {title: "1"}, {title: "47 m2 "}],
    price: "234.000€",
    sqm: "3456€ / sqm"
  },
  {
    title:"Berlin, Kreuzberg · Studio · Park view",
    location: "Berlin, Kreuzberg",
    features: [{title: "4"}, {title: "1"}, {title: "47 m2 "}],
    price: "234.000€",
    sqm: "3456€ / sqm"
  },
  {
    title: "Berlin, Neukölln · Studio · Renovated",
    location: "Berlin, Kreuzberg",
    features: [{title: "4"}, {title: "1"}, {title: "47 m2 "}],
    price: "234.000€",
    sqm: "3456€ / sqm"
  },
  {
    title: "Berlin, Charlottenburg · Apartment · Parking",
    location: "Berlin, Kreuzberg",
    features: [{title: "4"}, {title: "1"}, {title: "47 m2 "}],
    price: "234.000€",
    sqm: "3456€ / sqm"
  },
  {
    title:"Berlin, Steglitz · Villa · Garden · Pool",
    location: "Berlin, Kreuzberg",
    features: [{title: "4"}, {title: "1"}, {title: "47 m2 "}],
    price: "234.000€",
    sqm: "3456€ / sqm"
  },
  {
    title:"Berlin, Steglitz · Villa · Garden · Pool",
    location: "Berlin, Kreuzberg",
    features: [{title: "4"}, {title: "1"}, {title: "47 m2 "}],
    price: "234.000€",
    sqm: "3456€ / sqm"
  },
];

storiesOf("Modules/Carousels", module)
  .add("District", () => (
    <DistrictsModule title="What can we help you find?" subTitle="All countries" hasTitle>
      {list.map((item, index) => (
        <DistrictCard key={index} {...item} />
      ))}
    </DistrictsModule>
  ))
  .add("PopularCity", () => (
    <PopularCityModule title="Popular cities" subTitle="All cities" hasTitle>
      {list.map((item, index) => (
        <PopularCityCard key={index} {...item} />
      ))}
    </PopularCityModule>
  ))
  .add("PopularCountry", () => (
    <PopularCountryModule title="Popular countries" subTitle="All countries" hasTitle>
      {list.map((item, index) => (
        <PopularCountryCard key={index} {...item} />
      ))}
    </PopularCountryModule>
  ))
  .add("PropertyDetailsCard", () => (
    <PropertyDetailsModule title="Best Snow cabins for sale" subTitle="All cabins for sale" hasTitle>
      {propertyDetailsList.map((item, index) => (
        <PropertyDetailsCard
        {...item}
        carousel
        image={[
          "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg",
          "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg",
          "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg",
          "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg"
        ]}
        linkAs={<a href="https://govesta.co" style={{ width: 280, textDecoration: "none" }} />}
      />
      ))}
    </PropertyDetailsModule>
  ))