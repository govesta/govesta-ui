import React from "react";

import { storiesOf } from "@storybook/react";

import { FullCard } from "../src/components";

storiesOf("Components/FullCard", module).add("Default", () => (
  <div style={{ width: 236 }}>
    <FullCard
      title="Kreuzberg"
      subTitle="(34.555 Homes)"
      image="https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg"
      url="https://govesta.co"
    />
  </div>
));
