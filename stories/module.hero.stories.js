import React from "react";

import { storiesOf } from "@storybook/react";

import { HeroModule } from "../src/modules";

const images = [
  "https://images.unsplash.com/photo-1562862640-61aef0574543?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2108&q=80",
  "https://images.unsplash.com/photo-1562878562-c80950bd5340?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=934&q=80"
];

async function wait(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}

storiesOf("Modules/Hero", module).add("Default", () => (
  <HeroModule
    images={images}
    notification={{
      tag: "New",
      text: "You can search for <b>Neighborhoods</b> now"
    }}
    slogan="There’s no place like home.<br />Find your dream home."
    autocomplete={{
      
      placeholder: "Kreuzberg, Berlin",
      onSelect: value => {
        console.log(value);
      },
      onLoad: async () => {
        await wait(1000);
        return [
          { value: "chocolate", label: "Chocolate" },
          { value: "strawberry", label: "Strawberry" },
          { value: "vanilla", label: "Vanilla" }
        ];
      }
    }}
    history={{
      title: "Popular searches",
      items: [
        { label: "Kreuzberg", value: "kreuzberg" },
        { label: "Berlin", value: "berlin" },
        { label: "london", value: "London" }
      ],
      onClick: value => {
        console.log(value);
      }
    }}
  />
));
