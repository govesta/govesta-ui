import React from "react";

import { storiesOf } from "@storybook/react";

import { AlertBox } from "../src/components";

class Test extends React.Component {
  state = {
    show: false
  }
  componentDidMount() {
    this.setState({ show: true })
  }
  render() {
    return (
      <AlertBox
        show={this.state.show}
        title="Do you want to start your Govesta Listing"
        noText="No, not yet"
        onPressNo={() => {
          alert("no")
        }}
        yesText="Yes, lets Go"
        onPressYes={() => {
          alert("yes")
        }}
        onClose={() => {
          console.log("closed")
        }}
      />
    )
  }

}

storiesOf("Components/AlertBox", module).add("yellow", () => (
  <Test />
))

