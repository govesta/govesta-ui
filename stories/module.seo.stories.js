import React from "react";

import { storiesOf } from "@storybook/react";

import { SeoCard } from "../src/components";
import { SeoModule } from "../src/modules";

const images = [
  "https://www.countryflags.io/at/flat/64.png"
];

const list = [
  {
    title: "Govesta the global property search",
    text: "trivago’s hotel search allows users to compare hotel prices globally in just a few clicks from more than 400 booking sites for 1.8 million+ hotels in over 190 countries. With 1.4 billion visits annually to our site, travellers regularly use the hotel comparison to compare deals in the same city. Get information for weekend trips to cities like Liverpool or Birmingham and you can find the right hotel on trivago quickly and easily. New York City and its surrounding area are great for trips that are a week or longer with the numerous hotels available.",
    url: "https://govesta.co"
  },
  {
    title: "Find your ideal home on Govesta",
    text: "With trivago you can easily find your ideal hotel and compare prices from different websites. Simply enter where you want to go and your desired travel dates, and let our hotel search engine compare accommodation prices for you. To refine your search results, simply filter by price, distance (e.g. from the beach), star category, facilities and more. From budget hostels to luxury suites, trivago makes it easy to book online. You can search from a large variety of rooms and locations across the UK and Ireland, like Glasgow and Dublin to popular cities and holiday destinations abroad!",
    url: "https://govesta.co"
  },
  {
    title: "We connect you with local estate agents ",
    text: "Over 175 million aggregated hotel ratings and more than 19 million images allow you to find out more about where you're travelling. To get an extended overview of a hotel property, trivago shows the average rating and extensive reviews from other booking sites, e.g. Hotels.com, Expedia, Agoda, leading hotels, etc. trivago makes it easy for you to find information about your weekend trip to Amsterdam, including the ideal hotel for you.",
    url: "https://govesta.co"
  },
  {
    title: "How to buy a property",
    text: "trivago is a hotel search with an extensive price comparison. The prices shown come from numerous hotels and booking websites. This means that while users decide on trivago which hotel best suits their needs, the booking process itself is completed through the booking sites (which are linked to our website). By clicking on the “view deal” button, you will be forwarded onto a booking site where you can complete the reservation for the hotel deal found on trivago.",
    url: "https://govesta.co"
  },
  
 
];

storiesOf("Modules/Seo", module).add("Default", () => (
  <SeoModule>
    {list.map((item, index) => (
      <SeoCard key={index} {...item} />
    ))}
  </SeoModule>
));
