import React from "react";

import { storiesOf } from "@storybook/react";

import { PropertyDetails, PropertyDetailsCard } from "../src/components";

const item = {
  title:
    "Vacant two-room Altbau apartment with balcony views towards Viktoriapark in Berlin Kreuzberg",
  location: "Berlin, Kreuzberg",
  description: "The residential building with 20 apartments, 9 garages, and 7 parking spaces was extensively renovated in 2019 and is ideally suited as a profitable capital investment for investors and self-users alike. With living spaces varying between 34 to 60 m\u00b2, we are sure that here, you will find an apartment that will meet the expectations of all your personal wants and needs.\\n\\n\\nWe have a beautiful and renovated 2.5-bedroom-apartment on offer, available immediately. It consists of or lovely and bright living room with direct access to the balcony, as well as two additional rooms, which notably can be used as either bed- or study rooms.The kitchen offers space for a small dining area. A highlight of the apartment is the daylight bath and a separate storage room. \\n\\nIn order to protect the privacy of the tenants, this advertisement does not contain any interior photographs of the apartment. For example photographs of same, please view our Expos\u00e9. In order to receive further information on this apartment and its availability, please do not hesitate to contact Black Label Properties directly.",
  features: [{title: "Cheap"}, {title: "Apartment"}, {title: "4 rooms"}, {title: "Balcony"}],
  price: "234.000€",
  sqm: "3456€ / sqm",
  mapsApi: "",
  featuresList:[{title: "Balcony"}, {title: "New kitchen"}, {title: "Guest toilet"}, {title: "Garden"}],
  navIcons: {close: '', next: '', prev: ''},
  lat:52.518080,
  lng:13.314350
};

const cardItem = {
    title:"Berlin, Steglitz · Villa · Garden · Pool",
    location: "Berlin, Kreuzberg",
    features: [{title: "4"}, {title: "1"}, {title: "47 m2 "}],
    price: "234.000€",
    sqm: "3456€ / sqm"
  };

storiesOf("Components/PropertyDetails", module)
  .add("List Item", () => (
    <div>
      <PropertyDetails
        {...item}
        image={[
          "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg",
          "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg"
        ]}
        linkAs={<a className="property-details__link" href="https://govesta.co" target="_blank"/>}
      />
    </div>
  ))
  .add("PropertyCard", () => (
    <div style={{ width: 300 }}>
      <PropertyDetailsCard
        {...cardItem}
        carousel
        image={[
          "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg",
          "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg",
          "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg",
          "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg"
        ]}
        linkAs={<a href="https://govesta.co" style={{ width: 300, display: "inline-block", textDecoration: "none" }} />}
      />
    </div>
  ))