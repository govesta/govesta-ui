import React from "react";

import { storiesOf } from "@storybook/react";

import { PropertyTypesModule } from "../src/modules";
import { TypeCard } from "../src/components";

const images = [
  "https://images.unsplash.com/photo-1562862640-61aef0574543?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2108&q=80",
  "https://images.unsplash.com/photo-1562878562-c80950bd5340?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=934&q=80"
];

const list = [
  {
    title: "Apartment",
    image: images[0]
  },
  {
    title: "Apartment2",
    image: images[1]
  },
  {
    title: "Apartment1",
    image: images[0]
  },
  {
    title: "Apartment5",
    image: images[0]
  }
];

async function wait(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}

storiesOf("Modules/PropertyTypes", module).add("Default", () => (
  <PropertyTypesModule title="Property Types">
  	{list.map((item, index) => (
      <TypeCard key={index} {...item} />
    ))}
  </PropertyTypesModule>
));
