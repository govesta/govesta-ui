import React from "react";

import { storiesOf } from "@storybook/react";

import { GridGallery } from "../src/modules";

const images = [
  "https://images.unsplash.com/photo-1562862640-61aef0574543?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2108&q=80",
  "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg",
  "https://images.unsplash.com/photo-1562862640-61aef0574543?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2108&q=80",
  "https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg",
  "https://images.unsplash.com/photo-1562862640-61aef0574543?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2108&q=80",
  "https://images.unsplash.com/photo-1562862640-61aef0574543?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2108&q=80"
];

storiesOf("Modules/GridGallery", module).add("Default", () => (
  <GridGallery images={images} />
));
