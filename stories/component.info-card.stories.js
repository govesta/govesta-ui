import React from "react";

import { storiesOf } from "@storybook/react";

import { InfoText } from "../src/components";

storiesOf("Components/InfoText", module).add("Default", () => (
  <InfoText
    topText={<span>Choose your property type to get more relevant results for yourself. </span>}
    text={<span>In the last 100 days already more than 2.000 homes got sold in Berlin. </span>}
    icon={<i className="icon-twitter"></i>}
  />
))
