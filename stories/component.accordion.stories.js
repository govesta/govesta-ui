import React from "react";

import { storiesOf } from "@storybook/react";

import { AccordionComponent, GovestaText } from "../src/components";
import { LocationsModule  } from "../src/modules";



const LocationsList = [
  { text: "Alicante property" },
  { text: "Costa Blanca" },
  { text: "Côte d'Azur" },
  { text: "Alicante property" },
  { text: "Costa Blanca" },
  { text: "Côte d'Azur" },
  { text: "Alicante property" },
  { text: "Costa Blanca" },
  { text: "Côte d'Azur" },
  { text: "Alicante property" },
  { text: "Costa Blanca" },
  { text: "Côte d'Azur" },
  { text: "Alicante property" },
  { text: "Costa Blanca" },
  { text: "Côte d'Azur" },
  { text: "Alicante property" },
  { text: "Costa Blanca" },
  { text: "Côte d'Azur" },
  { text: "Alicante property" }
];

storiesOf("Components/Accordion", module).add("Default", () => (
  <div>
    <AccordionComponent title="Top Locations in germany">
    	<LocationsModule>
			    {LocationsList.map((item, index) => (
			      <GovestaText {...item} key={`location-item-${index}`}/>
			    ))}
			</LocationsModule>
    </AccordionComponent>
  </div>
))



