import React from "react";

import { storiesOf } from "@storybook/react";

import { ArticleCard, 
    CityCard, CountryItem, 
    DistrictCard, HelpCard, 
    PopularCountryCard,  PopularCityCard, PropertyType,
    TypeCard  } from "../src/components";


storiesOf("Components/Cards", module).add("ArticleCard", () => (
  <div style={{ width: 302 }}>
    <ArticleCard
      title="How much does it cost to maintain a property in Berlin?"
      image="https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg"
      text="The monthly fees for an owned apartment are higher than for a rented apartment, as an owner must bear costs that a tenant would not have to."
      tagTitle="Berlin"
      tagColor="red"
    />
  </div>
))
.add("CityCard", () => (
  <div style={{ width: 308 }}>
    <CityCard
      title="Apartments"
      image="https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg"
    />
  </div>
))
.add("CountryItem", () => (
  <div style={{ width: 308 }}>
    <CountryItem
      title="Portugal"
      count="4,241 properties"
      countryFlag="https://www.countryflags.io/at/flat/64.png"
      url="https://govesta.co"
    />
  </div>

))
.add("DistrictCard", () => (
  <div style={{ width: 154 }}>
    <DistrictCard
      title="Studio"
      image="https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg"
      url="https://govesta.co"
    />
  </div>
))
.add("HelpCard", () => (
  <div style={{ width: 242 }}>
    <HelpCard
      title="Buy a home"
      image="https://via.placeholder.com/84"
      subTitle="With over 1 million+ homes for sale available on the website, Trulia can match you with a house you will want to call home."
      buttonText="Find a home"
      buttonColor="green"
      onPress={()=>console.log('Find Home Clicked')}
    />
  </div>
))
.add("PopularCountryCard", () => (
  <div style={{ width: 242 }}>
    <PopularCountryCard
      title="Germany"
      subTitle="(24.567 Properties)"
      image="https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg"
      url="https://govesta.co"
    />
  </div>
))
.add("PopularCityCard", () => (
   <div style={{ width: 242 }}>
    <PopularCityCard
      title="Berlin"
      subTitle="Germany"
      image="https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg"
      url="https://govesta.co"
      cityDetails="658 penthouses, 521 terrace houses, 117 new developments , 52 studios, 37 luxury apartments"
    />
  </div>

))
.add("PropertyType", () => (
  <div style={{ width: 242 }}>
    <PropertyType
      title="Kreuzberg"
      subTitle="(34.555 Homes)"
      image="https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg"
      url="https://govesta.co"
    />
  </div>
))
.add("TypeCard", () => (
  <div style={{ width: 308 }}>
    <TypeCard
      title="Apartments"
      image="https://a0.muscache.com/pictures/9c878697-8553-42ba-9ed2-8e1e6204c13d.jpg"
    />
  </div>
))


