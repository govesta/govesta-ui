import React from "react";

import { storiesOf } from "@storybook/react";

import { SubscribeModule } from "../src/modules";


storiesOf("Modules/Subscribe", module).add("Default", () => (
  <SubscribeModule 
    title="Subscribte to the <br/> Govesta Newsletter"
    text="Berlin, Germany’s capital, dates to the 13th century. Reminders of the city's turbulent 20th-century history"
    image="https://via.placeholder.com/329" />    
));
