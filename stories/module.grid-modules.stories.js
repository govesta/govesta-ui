import React from "react";

import { storiesOf } from "@storybook/react";

import { CountryItem, GovestaText } from "../src/components";
import { CountryModule, LocationsModule } from "../src/modules";

const images = [
  "https://www.countryflags.io/at/flat/64.png"
];

const CountiesList = [
  {
    title: "Portugal",
    count: "4,241 properties",
    countryFlag: images[0],
    url: "https://govesta.co"
  },
  {
    title: "United States",
    count: "4,241 properties",
    countryFlag: images[0],
    url: "https://govesta.co"
  },
  {
    title: "Portugal",
    count: "4,241 properties",
    countryFlag: images[0],
    url: "https://govesta.co"
  },
  {
    title: "Austria",
    count: "4,241 properties",
    countryFlag: images[0],
    url: "https://govesta.co"
  },
  {
    title: "Germany",
    count: "4,241 properties",
    countryFlag: images[0],
    url: "https://govesta.co"
  },
  {
    title: "France",
    count: "4,241 properties",
    countryFlag: images[0],
    url: "https://govesta.co"
  },
  {
    title: "Portugal",
    count: "4,241 properties",
    countryFlag: images[0],
    url: "https://govesta.co"
  },
  {
    title: "Spain",
    count: "4,241 properties",
    countryFlag: images[0],
    url: "https://govesta.co"
  },
  {
    title: "Portugal",
    count: "4,241 properties",
    countryFlag: images[0],
    url: "https://govesta.co"
  },
  {
    title: "United Kingdom",
    count: "4,241 properties",
    countryFlag: images[0],
    url: "https://govesta.co"
  },
  {
    title: "Portugal",
    count: "4,241 properties",
    countryFlag: images[0],
    url: "https://govesta.co"
  },
  {
    title: "France",
    count: "4,241 properties",
    countryFlag: images[0],
    url: "https://govesta.co"
  },
  {
    title: "Portugal",
    count: "4,241 properties",
    countryFlag: images[0],
    url: "https://govesta.co"
  },
  {
    title: "Spain",
    count: "4,241 properties",
    countryFlag: images[0],
    url: "https://govesta.co"
  },
  {
    title: "Portugal",
    count: "4,241 properties",
    countryFlag: images[0],
    url: "https://govesta.co"
  },
 
];

const LocationsList = [
  { text: "Alicante property" },
  { text: "Costa Blanca" },
  { text: "Côte d'Azur" },
  { text: "Alicante property" },
  { text: "Costa Blanca" },
  { text: "Côte d'Azur" },
  { text: "Alicante property" },
  { text: "Costa Blanca" },
  { text: "Côte d'Azur" },
  { text: "Alicante property" },
  { text: "Costa Blanca" },
  { text: "Côte d'Azur" },
  { text: "Alicante property" },
  { text: "Costa Blanca" },
  { text: "Côte d'Azur" },
  { text: "Alicante property" },
  { text: "Costa Blanca" },
  { text: "Côte d'Azur" },
  { text: "Alicante property" }
];

storiesOf("Modules/GridModules", module)
.add("CountriesItemsGridWithFlag", () => (
  <CountryModule title="More countries" subTitle="All countries">
    {CountiesList.map((item, index) => (
      <CountryItem key={index} {...item} />
    ))}
  </CountryModule>
))
.add("LocationItemGrid", () => (
  <LocationsModule>
    {LocationsList.map((item, index) => (
      <GovestaText {...item} />
    ))}
  </LocationsModule>
));
