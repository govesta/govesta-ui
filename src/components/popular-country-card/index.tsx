// Libraries
import * as React from 'react';

// Style
import './style.styl'

interface IProps {
  image: string
  title?: string
  subTitle?: string
  className?: string
}

export { IProps as IPopularCountryCardProps }

const PopularCountryCard = ({ title, subTitle, image, className='' }: IProps) => {
  return (
    <div className={`g-popularcountry-card ${className}`}>
      <div>
        <div className="g-popularcountry-card__image" style={{ backgroundImage: `url(${image})` }} />
        <div className="g-popularcountry-card__content">
          {title && <span className="g-popularcountry-card__title">{title}</span>}
          {subTitle && <span className="g-popularcountry-card__subtitle">{subTitle}</span>}
        </div>
      </div>
    </div>
  )
}

const defaultProps: IProps = {
  image: null
}

PopularCountryCard.defaultProps = defaultProps;

export default PopularCountryCard;