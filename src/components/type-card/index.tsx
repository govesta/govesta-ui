// Libraries
import * as React from 'react';

// Style
import './style.styl'

interface IProps {
  image: string
  title?: string,
  className?: string
}

export { IProps as ITypeCardProps }

const TypeCard = ({ title, image, className='' }: IProps) => {
  return (
    <div className={`g-type-card ${className}`}>
        <div className="g-type-card__image" style={{ backgroundImage: `url(${image})` }} />
        
        {title && 
          <div className="g-type-card__title">{title}</div>
        }
        
    </div>
  )
}

const defaultProps: IProps = {
  image: null
}

TypeCard.defaultProps = defaultProps;

export default TypeCard;