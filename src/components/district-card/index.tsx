// Libraries
import * as React from 'react';

// Style
import './style.styl'

interface IProps {
  image: string
  title?: string
}

export { IProps as IDistrictCardProps }

const DistrictCard = ({ title, image }: IProps) => {
  return (
    <div className="g-district-card">
      <div>
        <div className="g-district-card__image" style={{ backgroundImage: `url(${image})` }} />
        <div className="g-district-card__content">
          {title && <span className="g-district-card__title">{title}</span>} 
        </div>
      </div>
    </div>
  )
}

const defaultProps: IProps = {
  image: null
}

DistrictCard.defaultProps = defaultProps;

export default DistrictCard;