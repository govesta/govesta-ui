// Libraries
import * as React from 'react';

// Components
import LightBox from '../../modules/lightbox-gallery';

// Style
import './style.styl'

interface IProps {
  linkAs?: any
  title?: string
  location?: string
  features?: Array<string>
  image: any
  price?: string
  sqm?: string
  carousel?: boolean
  smallOnMobile?: boolean
  lightbox?: Array<string>
}

export { IProps as IFullCardProps }

interface ICarouselProps {
  list: Array<string>
  lightBoxList: Array<string>
}

interface ICarouselState {
  list: Array<any>
  current: number
  lightbox: boolean
}

class Carousel extends React.Component<ICarouselProps, ICarouselState> {
  state = {
    lightbox: false,
    current: 0,
    list: this.props.list.map((url, i) => ({ url, loaded: i === 0 ? true : false }))
  }

  onPrev = (e: any) => {
    let { current } = this.state;
    e.preventDefault();
    if (current !== 0) {
      this.setState({ current: (current - 1) })
    }
  }

  onNext = (e: any) => {
    let { current, list } = this.state;
    e.preventDefault();
    if ((list.length - 1) > current) {
      current = current + 1;
      list[current].loaded = true;
      this.setState({ current, list })
    }
  }

  onClickImage = (e: any) => {
    const { lightBoxList } = this.props;
    if (lightBoxList.length > 0) {
      e.preventDefault();
      this.setState({ lightbox: true })
    }
  }

  render() {
    const { lightBoxList } = this.props;
    const { list, current, lightbox } = this.state;
    return (
      <>
        <div className="g-property-card__carousel">
          <div className="g-property-card__carousel__list" style={{ transform: `translateX(-${100 * current}%)` }} onClick={this.onClickImage}>
            {list.map((image, i) => {
              const { url, loaded } = image;
              const style: any = {};
              if (loaded) {
                style.backgroundImage = `url(${url})`
              }
              return (
                <div key={`carousel-${i}`} style={style} />
              )
            })}
          </div>
          <span className="slider-arrow slider-arrow-prev" onClick={this.onPrev}>
            <i className="icon-left"></i>
          </span>
          <span className="slider-arrow slider-arrow-next" onClick={this.onNext}>
            <i className="icon-right"></i>
          </span>
        </div>
        {lightBoxList.length > 0 && (
          <LightBox
            images={lightBoxList}
            show={lightbox}
            onClose={() => {
              this.setState({ lightbox: false })
            }}
          />
        )}
      </>
    )
  }
}

const PropertyCard = ({ linkAs, image, location, title, features, price, sqm, carousel, smallOnMobile, lightbox }: IProps) => {
  const cardImage = carousel ? <Carousel list={image} lightBoxList={lightbox} /> : (<div className="g-property-card__image" style={{ backgroundImage: `url(${Array.isArray(image) ? image[0] : image})` }} />);
  const cardContent = (
    <React.Fragment>
      {location && <span className="g-property-card__location">{location}</span>}
      {title && <span className="g-property-card__title">{title}</span>}
      {features.length != 0 && (
        <div className="g-property-card__features">
          {features.join(', ')}
        </div>
      )}
      <div className="g-property-card__footer">
        {price && <span className="g-property-card__price">{price}</span>}
        {sqm && <span className="g-property-card__sqm">{sqm}</span>}
      </div>
    </React.Fragment>
  )
  if (lightbox && lightbox.length > 0) {
    return (
      <div className={`g-property-card ${smallOnMobile && 'small'}`}>
        {cardImage}
        {React.cloneElement(linkAs, {
          className: "g-property-card__content",
          children: cardContent
        })}
      </div>
    )
  }
  return React.cloneElement(linkAs, {
    className: `g-property-card ${smallOnMobile && 'small'}`,
    children: (
      <React.Fragment>
        {cardImage}
        {cardContent}
      </React.Fragment>
    )
  })
}

const defaultProps: IProps = {
  image: null,
  features: [],
  carousel: true,
  smallOnMobile: false,
  lightbox: []
}

PropertyCard.defaultProps = defaultProps;

export default PropertyCard;