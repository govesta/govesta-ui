// Libraries
import * as React from 'react';

// Style
import './style.styl'

interface IProps {
  image: string
  title?: string
  subTitle?: string
  className?: string
}

export { IProps as IPropertyTypeProps }

const PropertyType = ({ title, subTitle, image, className='' }: IProps) => {
  return (
    <div className={`g-property-type ${className}`}>
      <div>
        <div className="g-property-type__image" style={{ backgroundImage: `url(${image})` }} />
        <div className="g-property-type__content">
          {title && <span className="g-property-type__title">{title}</span>}
          {subTitle && <span className="g-property-type__subtitle">{subTitle}</span>}
        </div>
      </div>
    </div>
  )
}

const defaultProps: IProps = {
  image: null
}

PropertyType.defaultProps = defaultProps;

export default PropertyType;