// Libraries
import * as React from 'react';

// Style
import './style.styl'

interface IProps {
  text?: any
  className?: string
}

export { IProps as IGTextProps }

const GovestaText = ({  text, className='' }: IProps) => {
  return (

    <div className={`g-text ${className}`}>
    	{text}
    </div>
  )
}

const defaultProps: IProps = {

}

GovestaText.defaultProps = defaultProps;

export default GovestaText;



