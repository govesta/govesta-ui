// Libraries
import * as React from 'react';
import ReactSelect, { components } from 'react-select'

// Style
import './style.styl'

interface ISettings {
  isSearchable: boolean
  isClearable: boolean
  isLoading: boolean
}

interface IProps {
  error?: boolean
  options: Array<any>
  settings: ISettings
  placeholder?: any
  onChange?: any
  value?: any
}

const NoOptionsMessage = (): any => null
const IndicatorSeparator = (): any => null
const Option = (props: any) => {
  const { data } = props;
  return (
    <components.Option {...props}>
      <div className="g-form-select-option">
        {data.image && <img src={data.image} />}
        <span>{props.label}</span>
      </div>
    </components.Option>
  )
}

const SingleValue = (props: any) => {
  const { data } = props;
  return (
    <components.SingleValue {...props}>
      <div className="g-form-select-option">
        {data.image && <img src={data.image} />}
        <span>{data.label}</span>
      </div>
    </components.SingleValue>
  )
}

const defaultSettings = {
  isSearchable: false,
  isClearable: false,
  isLoading: false
}

const Select = ({ value, settings, options, placeholder, onChange, error }: IProps) => {
  const selectedValue = value ? (value.value ? value : options.find(o => o.value === value)) : null;
  return (
    <ReactSelect
      value={selectedValue}
      options={options}
      components={{ NoOptionsMessage, IndicatorSeparator, Option, SingleValue }}
      placeholder={placeholder}
      onChange={onChange}
      styles={{
        container: (base: any) => { return { ...base, flex: 1 } },
        menuList: (base: any) => { return { ...base, padding: 0 } },
        menu: (base: any, { options, isLoading }) => { return { ...base, border: (options.length !== 0 || isLoading) ? "1px solid #CED2D4" : 0, boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.05)", borderRadius: 4, overflow: "hidden" } },
        option: (base: any, { isFocused, isSelected }) => { return { ...base, paddingTop: 15, paddingBottom: 15, paddingLeft: 20, paddingRight: 20, fontSize: 15, color: "#4B5169", backgroundColor: (isSelected || isFocused) && "#F0F5FB" } },
        control: (base: any) => { return { ...base, border: `1px solid ${error ? "#FF5A5A" : "#CED2D4"}`, height: 42, color: "#25263B", boxShadow: 'none', paddingLeft: 7 } },
        placeholder: (base: any) => { return { ...base, color: "#4B5169", fontSize: 15, lineHeight: "22px" } },
        singleValue: (base: any) => { return { ...base, color: "#4B5169", fontSize: 15, lineHeight: "22px" } },
        input: (base: any) => { return { ...base, color: "#4B5169", fontSize: 15, lineHeight: "22px" } }
      }}
      {...{ ...defaultSettings, ...settings }}
    />
  )
}

const defaultProps: IProps = {
  options: [],
  settings: defaultSettings
}

Select.defaultProps = defaultProps;

export default Select;