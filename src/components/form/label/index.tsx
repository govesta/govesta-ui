// Libraries
import * as React from 'react';

// Style
import './style.styl'

interface IProps {
  children?: any
}

const Label = ({ children }: IProps) => {
  return (
    <label className="g-form-label">{children}</label>
  )
}

const defaultProps: IProps = {

}

export default Label;