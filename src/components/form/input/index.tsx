// Libraries
import * as React from 'react';
import NumberFormat from 'react-number-format';

// Style
import './style.styl'

interface IProps {
  placeholder?: string
  suffix?: any
  onChange?: Function
  value?: string
  error?: boolean
  numeric?: boolean
}

const Input = ({ placeholder, suffix, onChange, value, error, numeric }: IProps) => {
  return (
    <div className={`g-form-input ${error && 'error'}`}>
      {(() => {
        if (numeric) {
          return (
            <NumberFormat
              value={value}
              placeholder={placeholder}
              onChange={(e) => { onChange(e.target.value) }}
            />
          )
        }
        return (
          <input
            value={value}
            placeholder={placeholder}
            onChange={(e) => { onChange(e.target.value) }}
          />
        )
      })()}
      {suffix && <span className="g-form-input__suffix">{suffix}</span>}
    </div>
  )
}

const defaultProps: IProps = {
  numeric: false
}

export default Input;