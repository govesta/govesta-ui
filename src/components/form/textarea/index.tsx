// Libraries
import * as React from 'react';

// Style
import './style.styl'

interface IProps {
  placeholder?: string
  onChange?: Function
  value?: string
  error?: boolean
}

const TextArea = ({ placeholder, onChange, value, error }: IProps) => {
  return (
    <textarea
      placeholder={placeholder}
      className={`g-form-textarea ${error && 'error'}`}
      onChange={(e) => { onChange(e.target.value) }}
      value={value}
    />
  )
}

const defaultProps: IProps = {

}

export default TextArea;