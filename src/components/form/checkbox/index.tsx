// Libraries
import * as React from 'react';

// Style
import './style.styl';

interface IProps {
  children: any
  onChange?: Function
  value?: any
  error?: boolean
}

const Check = ({ children = null, value, onChange, error = false }:IProps) => {
  return (
    <label className={`g-form-check ${error && 'error'}`}>
      <input type="checkbox" checked={value} onChange={(e) => { onChange(e.target.checked); }} />
      <span className="g-form-check__icon">
        <i className="icon-check"></i>
      </span>
      <span className="g-form-check__text">{children}</span>
    </label>
  )
}

export default Check;