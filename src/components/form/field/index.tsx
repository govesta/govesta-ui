// Libraries
import * as React from 'react';

// Style
import './style.styl'

// Components
import Label from '../label'

interface IProps {
  label?: any
  children?: any
  error?: any
}

const Field = ({ label, children, error }: IProps) => {
  return (
    <div className="g-form-field">
      {label && <Label>{label}</Label>}
      <div className="g-form-field__content">
        {React.cloneElement(children, { error: !!error })}
      </div>
      {error && <span className="g-form-error">{error}</span>}
    </div>
  )
}

const defaultProps: IProps = {

}

export default Field;