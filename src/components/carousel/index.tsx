// Libraries
import * as React from 'react';
import Slider, { Settings } from "react-slick";

// Style
import './style.styl'

interface IProps extends Settings {
    className?: string
    children?: any
    current?: number
}

interface IState {
    current?: number
}

const Item = ({ children }: any) => {
    return children;
}

class Container extends React.Component<IProps, any> {
    ref: any = null
    static defaultProps: IProps = {
        dots: true,
        arrows: false,
        infinite: false
    }
    state: IState = {
        current: this.props.current || 0
    }
    goPrev = () => {
        this.ref.slickPrev()
    }
    goNext = () => {
        this.ref.slickNext()
    }
    go = (item: number) => {
        this.ref.slickGoTo(item, true)
    }
    render() {
        const { current } = this.state;
        const { children, className, ...settings } = this.props;
        return (
            <div className={`g-carousel ${className}`}>
                <Slider initialSlide={current} ref={(ref) => { this.ref = ref; }} {...settings}>
                    {children}
                </Slider>
            </div>
        );
    }
}

export {
    Item,
    Container
};