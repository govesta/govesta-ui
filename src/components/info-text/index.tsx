// Libraries
import * as React from 'react';

// Style
import './style.styl'

interface IProps {
  icon?: string
  text?: any
  topText?: any
}

export { IProps as IFullCardProps }

const InfoText = ({ topText, text, icon }: IProps) => {
  return (
    <div className="g-info-text">
      {topText && <div className="g-info-text__top">{topText}</div>}
      <div className="g-info-text__container">
        {icon}
        <span className="g-info-text__content">{text}</span>
      </div>
    </div>
  )
}

const defaultProps: IProps = {

}

InfoText.defaultProps = defaultProps;

export default InfoText;