// Libraries
import * as React from 'react';

// Style
import './style.styl'

interface IProps {
  image: string
  title?: string
  subTitle?: string
  cityDetails?: string
  className?: string
}

export { IProps as IPopularCityCardProps }

const PopularCityCard = ({ title, subTitle, image , cityDetails='', className=''}: IProps) => {
  cityDetails="658 penthouses, 521 terrace houses, 117 new developments , 52 studios, 37 luxury apartments";

  return (
     <div className={`g-popularcity-card ${className}`}>
	    <div className={`g-popularcity-card__card`}>
	      <div>
	        <div className="g-popularcity-card__card__image" style={{ backgroundImage: `url(${image})` }} />
	        <div className="g-popularcity-card__card__content">
	          {title && <span className="g-popularcity-card__card__title">{title}</span>}
	          {subTitle && <span className="g-popularcity-card__card__subtitle">{subTitle}</span>}
	        </div>
	      </div>
	    </div>
    	<div className="g-popularcity-card__details">
    		{cityDetails && <div className="g-popularcity-card__details__citydetails">{cityDetails}</div>}
    	</div>

    </div>
    
  )
}

const defaultProps: IProps = {
  image: null
}

PopularCityCard.defaultProps = defaultProps;

export default PopularCityCard;