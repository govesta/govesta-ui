// Libraries
import * as React from 'react';

// Style
import './style.styl'

type Color = 'yellow' | 'orange' | 'red' | 'brand' | 'brand-dark' | 'brand-light' | 'blue'


import {Tag} from '../'

interface IProps {
  image: string
  title?: string
  text?: string
  tagTitle?: string
  className?: string
  tagColor?: Color
}

export { IProps as IArticleCardProps }

const ArticleCard = ({ title, text, image, tagTitle, tagColor, className='' }: IProps) => {
  return (
    <div className={`g-article-card ${className}`}>
        <div className="g-article-card__image" style={{ backgroundImage: `url(${image})` }} />
        <div><Tag className="g-article-card__tag" content={tagTitle} color={tagColor} /></div>
        <div className="g-article-card__content">
        {title && 
          <div className="g-article-card__title">{title}</div>
        }
        {text && 
          <div className="g-article-card__subtitle">{text}</div>
        }
          </div>
    </div>
  )
}

const defaultProps: IProps = {
  image: null
}

ArticleCard.defaultProps = defaultProps;

export default ArticleCard;