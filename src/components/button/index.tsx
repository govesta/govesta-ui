// Libraries
import * as React from 'react';

// Style
import './style.styl'

type Type = 'button' | 'submit' | 'reset'
type Size = 'small' | 'medium' | 'large'
type Theme = 'brand' | 'brand-border' | 'white' | 'white-border' | 'black-border' | 'basic-five'

interface IProps {
    text?: string
    type?: Type
    size?: Size
    href?: string
    onClick?: any
    children?: React.ReactChildren
    theme?: Theme
    className?: string
    block?: boolean
    linkAs?: any
    target?: string
    disabled?: boolean
    loading?: boolean
}

const Button = ({ disabled, type, href, target = "_self", onClick, children, theme, className, block, linkAs, size, loading }: IProps) => {
    const concatClass = `g-button g-button--${theme} ${loading && 'g-button--loading'} g-button--${size} ${className} ${block && 'block'} ${disabled && 'g-button--disabled'}`;
    if (linkAs) {
        return React.cloneElement(linkAs, { className: concatClass, children: <span>{children}</span> });
    }
    if (href) {
        return (
            <a onClick={onClick} href={href} target={target} className={concatClass}>{children}</a>
        )
    }
    return (
        <button onClick={onClick} className={concatClass} type={type}>{children}</button>
    )
}

const defaultProps: IProps = {
    block: false,
    size: 'small',
    theme: 'brand'
}

Button.defaultProps = defaultProps;

export default Button;
