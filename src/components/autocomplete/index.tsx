// Libraries
import * as React from 'react';
import AsyncSelect from 'react-select/async';
import { components } from 'react-select'

// Style
import './style.styl';

// Interfaces
interface IAutocompleteProps {
  shadow?: boolean
  className?: string
  placeholder?: string
  isClearable?: boolean
  value?: any
  onSelect?: Function
  onChange?: Function
  onKeyDown?: any
  onLoad?: Function
  size?: string
  icon?: string
  optionIcon?: string
}

interface IState {

}

export { IAutocompleteProps }

const NoOptionsMessage = (): any => null
const IndicatorSeparator = (): any => null

export default class Location extends React.Component<IAutocompleteProps, IState> {

  constructor(props: IAutocompleteProps) {
    super(props);

    this.loadOptions = this.loadOptions.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  onChange(value: any) {
    this.props.onSelect(value);
  }

  async loadOptions(inputValue: string, callback: Function) {
    const { onLoad } = this.props;
    const list = await onLoad(inputValue);
    callback(list);
  }

  getFontSize(size: string) {
    return size === "small" ? 15 : 18;
  }

  getHeight(size: string) {
    return size === "small" ? 40 : 52;
  }

  render() {
    const { isClearable = true, onKeyDown, placeholder, value, className='', size = "medium", shadow = true, icon, optionIcon } = this.props;
    const DropdownIndicator = (props: any) => {
      if (!icon || props.hasValue) {
        return null;
      }
      return (
          <components.DropdownIndicator {...props}>
            <span className={`g-autocomplete__icon ${size}`}>
              <i className="icon-search" />
            </span>
          </components.DropdownIndicator>
      );
    };


    const IndicatorsContainer = (props:any) => {
      return (
        <div className={`g-autocomplete__indicators ${size}`}>
          <components.IndicatorsContainer {...props}  />
        </div>
      );
    };

    const Option = (props: any) => {
      return (
        <components.Option {...props}>
          <span className="g-autocomplete__option">
            <span className="g-autocomplete__option__text">{props.label}</span>
            {optionIcon && <i className={optionIcon} />}
          </span>
        </components.Option>
      );
    };
    return (
      <div className={`g-autocomplete ${className} ${size} ${!shadow && 'noshadow'}`}>
        <AsyncSelect
          value={value}
          placeholder={placeholder}
          isClearable={isClearable}
          onChange={this.onChange}
          components={{ IndicatorsContainer, DropdownIndicator, NoOptionsMessage, Option, IndicatorSeparator }}
          cacheOptions
          loadOptions={this.loadOptions}
          onKeyDown={(e: any) => { onKeyDown && onKeyDown(e); }}
          styles={{
            container: (base: any) => { return { ...base, flex: 1} },
            menuList: (base: any) => { return { ...base, padding: 0 } },
            menu: (base: any, { options, isLoading }) => { return { ...base, border: (options.length !== 0 || isLoading) ? "1px solid #CED2D4" : 0, boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.05)", borderRadius: 4, overflow: "hidden" } },
            option: (base: any, { isFocused, isSelected }) => { return { ...base, paddingTop: 15, paddingBottom: 15, paddingLeft: 20, paddingRight: 20, fontSize: 15, color: "#4B5169", backgroundColor: (isSelected || isFocused) && "#F0F5FB" } },
            control: (base: any) => { return { ...base, border: 0, height: this.getHeight(size), color: "#25263B", boxShadow: 'none', paddingLeft: 10 } },
            placeholder: (base: any) => { return { ...base, color: "#4B5169", fontSize: this.getFontSize(size), lineHeight: "22px" } },
            singleValue: (base: any) => { return { ...base, color: "#4B5169", fontSize: this.getFontSize(size), lineHeight: "22px" } },
            input: (base: any) => { return { ...base, color: "#4B5169", fontSize: this.getFontSize(size), lineHeight: "22px" } },
            clearIndicator:  (base: any) => { return { ...base, color: "#fff" } },
          }}
        />
      </div>
    )
  }
}
