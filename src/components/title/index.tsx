// Libraries
import * as React from 'react';

// Style
import './style.styl'

interface IProps {
  title?: any
  className?: string
}

export { IProps as IGTitleProps }

const GovestaTitle = ({ title, className='' }: IProps) => {
  return (

    <div className={`g-title ${className}`}>
    	{title}	
    </div>
  )
}

const defaultProps: IProps = {

}

GovestaTitle.defaultProps = defaultProps;

export default GovestaTitle;



