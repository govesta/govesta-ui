// Libraries
import * as React from 'react';

// Style
import './style.styl'

interface IProps {
  title?: any
  text?: any
  className?: string
}

export { IProps as ISeoCardProps}

const SeoCard = ({ title, text, className='' }: IProps) => {
  return (

    <div className={`g-seo-card ${className}`}>
    	<div className="g-seo-card__content">
	      {title && <div className="g-seo-card__title">{title}</div>}
	      {text && <div className="g-seo-card__description">{text}</div>}
      	</div>
    </div>
  )
}

const defaultProps: IProps = {

}

SeoCard.defaultProps = defaultProps;

export default SeoCard;




