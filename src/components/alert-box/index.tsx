// Libraries
import * as React from 'react';

// Style
import './style.styl'

// Components
import Button from '../button';

// Interfaces
interface IProps {
  show?: boolean
  title?: any
  noText?: any
  yesText?: any
  onPressNo?: Function
  onPressYes?: Function
  onClose?: Function
}

interface IState {

}

class AlertBox extends React.Component<IProps, IState> {
  ref: any = null;
  container: any = null;

  componentDidMount() {
    window.addEventListener("keyup", this.escFunction, true)
  }

  componentWillReceiveProps(nextProps: IProps) {
    if (nextProps.show !== this.props.show) {
      if (nextProps.show) {
        document.body.style.overflow = "hidden"
      } else {
        document.body.style.overflow = ""
      }
    }
  }

  componentWillMount() {
    window.removeEventListener("keyup", this.escFunction)
  }

  escFunction = (e: any) => {
    const { show, onClose } = this.props;
    if (show && onClose && (e.keyCode === 27)) {
      onClose();
    }
  }

  handleOutside = (e: any) => {
    const { onClose } = this.props;
    if (!this.ref.contains(e.target)) {
      onClose && onClose()
    }
  }

  render() {
    const { title, noText, yesText, show, onPressNo, onPressYes, onClose } = this.props;
    if (!show) {
      return null;
    }
    return (
      <div className="g-alertbox" onClick={this.handleOutside}>
        <div className="g-alertbox__inner" ref={(ref) => { this.ref = ref; }}>
          <div className="g-alertbox__title">{title}</div>
          <div className="g-alertbox__actions">
            <Button size="medium" theme="basic-five" onClick={onPressNo}>
              {noText}
            </Button>
            <Button size="medium" onClick={onPressYes}>
              {yesText}
            </Button>
          </div>
        </div>
      </div>
    )
  }
}

export default AlertBox;