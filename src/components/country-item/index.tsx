// Libraries
import * as React from 'react';

// Style
import './style.styl'

interface IProps {
  countryFlag: string
  title?: string
  count?: string
  className?: string
}

export { IProps as ICountryItemProps }

const CountryItem = ({ title, count, countryFlag, className='' }: IProps) => {
  return (
    <div className={`g-country-item ${className}`}>
      <div className="g-country-item__image" style={{ backgroundImage: `url(${countryFlag})` }} />
      <div className="g-country-item__content">
        <span className="g-country-item__title">{title}</span>
        <span className="g-country-item__sub">{count}</span>
      </div>
    </div>
  )
}

const defaultProps: IProps = {
  countryFlag: null
}

CountryItem.defaultProps = defaultProps;

export default CountryItem;