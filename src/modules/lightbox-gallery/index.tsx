// Libraries
import * as React from 'react';

// Style
import './style.styl';

// Componnets
import * as Carousel from '../../components/carousel';

// Interfaces
interface IProps {
  images: Array<string>
  show: boolean
  initial?: number
  onClose?: Function
}
interface IState {
  currentSlide: number
}

class LightBoxGallery extends React.Component<IProps, IState> {

  carouselRef: any = null;
  containerRef: any = null;

  state = {
    currentSlide: this.props.initial || 0
  }

  componentDidMount() {
    window.addEventListener("keydown", this.onKeyDown);
  }

  componentWillReceiveProps(nextProps: IProps) {
    if (nextProps.show !== this.props.show && nextProps.show) {
      setTimeout(() => {
        if (this.containerRef) {
          const el = this.containerRef.querySelector(".slick-list");
          if (el) {
            el.setAttribute("tabindex", 0);
            el.focus();
          }
        }
      }, 100);
    }
  }

  componentWillUnmount() {
    window.removeEventListener("keydown", this.onKeyDown)
  }

  onKeyDown = (e: any) => {
    const { onClose } = this.props;
    if (e.keyCode === 27) {
      onClose();
    }
  }

  render() {
    const { images, show, onClose } = this.props;
    const { currentSlide } = this.state;
    if (!show) {
      return null;
    }
    return (
      <div className="g-lightbox-gallery__full" ref={(ref) => { this.containerRef = ref }}>
        <i className="icon-close lightbox-close" onClick={() => { onClose() }} />
        <Carousel.Container current={currentSlide} ref={(ref) => { this.carouselRef = ref; }} className="g-lightbox-gallery__full__container" dots={false}>
          {images.map((img, index) => {
            return (
              <Carousel.Item key={`gallery-${index}`}>
                <div style={{ backgroundImage: `url(${img})` }} />
              </Carousel.Item>
            )
          })}
        </Carousel.Container>
        <span className="icon-left lightbox-arrow lightbox-arrow-left" onClick={() => { this.carouselRef.goPrev() }} />
        <span className="icon-right lightbox-arrow lightbox-arrow-right" onClick={() => { this.carouselRef.goNext() }} />
      </div>
    )
  }
}

export default LightBoxGallery