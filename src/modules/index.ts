import ArticleModule from  './article';
import HelpModule from  './help';
import CountryModule from  './country';
import SeoModule from  './seo';

import HeroModule from './hero';
import FullCardModule from './full-card';
import FilterCardModule from './filter-card';
import BannerModule from './banner';
import LogoModule from './logo';
import GridGallery from './grid-gallery';
import CityDetailsModule from './city-details';
import PropertyTypesModule from './property-types';

import CarouselModule from './carousel-wrapper';
import DistrictsModule from './districts';
import PopularCityModule from './popular-city';
import PopularCountryModule from './popular-country';

import LocationsModule from './locations';

import SearchModule from './search';
import PropertyDetailsModule from './property-details';

import SubscribeModule from './subscribe';

export {
	ArticleModule,
	HelpModule,
    CountryModule,
    SeoModule,
    GridGallery,
    FilterCardModule,
    FullCardModule,
    HeroModule,
    BannerModule,
    LogoModule,
    CityDetailsModule,
    PropertyTypesModule,
    LocationsModule,
    SearchModule,
    SubscribeModule,

    CarouselModule,
    DistrictsModule, 
    PopularCityModule,
    PopularCountryModule,
    PropertyDetailsModule, 
}