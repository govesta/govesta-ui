// Libraries
import * as React from 'react';

// Style
import './style.styl'

// Components
import { Button } from '../../components';

interface IProps {
    image?: string
    title?: any
    smallText?: any
    description?: any
    linkText?: any
    link?: string
    bigText?: any
    linkAs?: any
    full?: boolean
    dark?: boolean
}

const Banner = ({ image, smallText, title, description, bigText, link, linkText, linkAs, full, dark }: IProps) => {
    const HTML = (
        <div className="row">
            <div className="col-md-7 col-sm-12 col-xs-12">
                {smallText && <div className="banner-module__small-text">{smallText}</div>}
                {title && <div className="banner-module__title">{title}</div>}
                {bigText && <div className="banner-module__big-text">{bigText}</div>}
                {description && <div className="banner-module__description">{description}</div>}
                {linkText && <Button theme={dark ? "brand" : "white"} href={link} linkAs={linkAs}>{linkText}</Button>}
            </div>
        </div>
    );
    const className = `banner-module ${full && 'banner-module--full'} ${dark && 'banner-module--dark'}`
    if (full) {
        return (
            <div className={className} style={{ backgroundImage: `url(${image})` }}>
                <div className="container">
                    {HTML}
                </div>
            </div>
        )
    }
    return (
        <div>
            <div className="container">
                <div className="row">
                    <div className="col-xs-12" style={{ padding: 0 }}>
                        <div className={className} style={{ backgroundImage: `url(${image})` }}>
                            {HTML}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

const defaultProps: IProps = {
    full: false,
    dark: false
}

Banner.defaultProps = defaultProps;

export default Banner;