// Libraries
import * as React from 'react';

// Style
import './style.styl'

import { ModuleTitle } from '../../utils';


// Components

interface IProps {
    children?: any
}

const LocationsModule = ({ children  }: IProps) => {
    return (
        <div className="locations-module">
            <div className="container">
                <div className="row">
                    <div className="col-xs-12" >
                        <div className="locations-module__cardswrapper">
                            {React.Children.map(children, (child, index) => {
                                return (
                                    <div className="locations-module__item" key={`location-item-${index}`}>
                                        {child}
                                    </div>
                                )
                            })}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    )
}

export default LocationsModule;