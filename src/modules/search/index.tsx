// Libraries
import * as React from 'react';

// Style
import './style.styl'

// Components
import { Autocomplete, IAutocompleteProps } from '../../components';


interface IProps {
    autocomplete?: IAutocompleteProps
    image: string
    slogan?: any
}

const Search = ({ slogan, autocomplete, image }: IProps) => {
    return (
        <div className="search-module">
            <div>
                <div className="search-module__image" style={{ backgroundImage: `url(${image})` }} />
                <div className="search-module__content">
                    <div className="container">
                        <div className="row middle-xs search-row">
                            <div className="col-md-8 col-sm-8 col-xs-8">
                                {slogan && <h1 className="search-module__title">{slogan}</h1>}
                                {
                                    autocomplete && (
                                        <Autocomplete
                                            icon="icon-search"
                                            className="search-module__autocomplete"
                                            {...autocomplete}
                                        />
                                    )
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {
                /*<div className="container">
                    <div className="row middle-xs">
                        <div className="col-md-8 col-sm-8 col-xs-8">
                          
                            <h1>{slogan}</h1>
                            {
                                    autocomplete && (
                                    <Autocomplete
                                        icon="icon-search"
                                        className="search-module__autocomplete"
                                        {...autocomplete}
                                    />
                                    )
                            }
                            
                        </div>
                        
                    </div>
                </div>*/

            }

        </div>
    )
}

const defaultProps: IProps = {
    image: null
}

Search.defaultProps = defaultProps;

export default Search;

