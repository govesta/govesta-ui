// Libraries
import * as React from 'react';

// Style
import './style.styl'

// Components
import { ModuleTitle } from '../../utils';


interface IProps {
  children?: any
}

const LogoModule = ({ children }: IProps) => {
  return (
    <div className="logo-module">
      <div className="container">
        <div className="row">
          <div className="col-xs-12">
            <div className="logo-module__container">
              {React.Children.map(children, (child, index) => {
                return (
                  <div className="logo-module__item" key={`full-card-${index}`}>
                    {child}
                  </div>
                )
              })}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default LogoModule;