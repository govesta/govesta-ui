// Libraries
import * as React from 'react';

// Style
import './style.styl';

// Componnets
import LightBoxGallery from '../lightbox-gallery'

// Interfaces
interface IProps {
  lightbox: Array<string>
  images: Array<string>
}
interface IState {
  isCarousel: boolean
  currentSlide: number
}

class GridGallery extends React.Component<IProps, IState> {
  carouselRef: any = null;

  state = {
    currentSlide: 0,
    isCarousel: false,
  }

  openCarousel = (currentSlide: number) => {
    this.setState({ currentSlide, isCarousel: true })
  }

  render() {
    const { images, lightbox } = this.props;
    const { isCarousel, currentSlide } = this.state;
    return (
      <>
        <div className={`g-grid-gallery grid${(images.length < 5 ? (images.length) : 5)}`}>
          {images.map((img, index) => {
            return (
              <div
                key={`gallery-${index}`}
                style={{ backgroundImage: `url(${img})`, display: index < 5 ? "block" : "none" }}
                onClick={() => { this.openCarousel(index) }}
              />
            )
          })}
        </div>
        <LightBoxGallery
          show={isCarousel}
          images={lightbox}
          initial={currentSlide}
          onClose={() => {
            this.setState({ isCarousel: false })
          }}
        />
      </>
    )
  }
}

export default GridGallery