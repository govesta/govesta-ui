// Libraries
import * as React from 'react';

// Style
import './style.styl'

// Components
import { Carousel, Button, Tag, Autocomplete, IAutocompleteProps } from '../../components';

interface IHistoryItem {
    label: any
    value: any
}

interface IHistory {
    title: string,
    items: Array<IHistoryItem>
    onClick?: Function
}

interface INotification {
    tag: string,
    text: any
}

interface IProps {
    autocomplete?: IAutocompleteProps
    notification?: INotification
    images: Array<string>
    slogan?: any
    history?: IHistory
}

const Hero = ({ images, notification, slogan, autocomplete, history }: IProps) => {
    return (
        <div className="hero-module">
            <div className="container">
                <div className="row middle-xs">
                    <div className="col-md-5 col-sm-6 col-xs-12">
                        {notification && (
                            <span className="hero-module__notification">
                                <Tag color="orange" content={notification.tag} />
                                <span className="hero-module__notification__text">
                                    {notification.text}
                                </span>
                            </span>
                        )}
                        <h1>{slogan}</h1>
                        {
                                autocomplete && (
                                <Autocomplete
                                    icon="icon-search"
                                    className="hero-module__autocomplete"
                                    {...autocomplete}
                                />
                                )
                        }
                        {history.items.length != 0 && (
                            <div className="hero-module__history">
                                <span className="hero-module__history__title">{history.title}</span>
                                {history.items.map((item) => (
                                    <Button theme="brand-border" key={`history-${item.value}`} onClick={() => { history.onClick(item.value) }}>{item.label}</Button>
                                ))}
                            </div>
                        )}
                    </div>
                    <div className="col-md-7 col-sm-6 col-xs-12 last-md last-sm first-xs">
                        <Carousel.Container className="hero-module__carousel">
                            {images.map((image, index) => (
                                <Carousel.Item key={`carousel-${index}`}>
                                    <div
                                        style={{
                                            backgroundImage: `url(${image})`
                                        }}
                                    />
                                </Carousel.Item>
                            ))}
                        </Carousel.Container>
                    </div>
                </div>
            </div>
        </div>
    )
}

const defaultProps: IProps = {
    images: [],
    history: { title: null, items: [] }
}

Hero.defaultProps = defaultProps;

export default Hero;