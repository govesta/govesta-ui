// Libraries
import * as React from 'react';

// Style
import './style.styl'

import { ModuleTitle } from '../../utils';


// Components

interface IProps {
    title?: any
    children?: any
}

const SeoModule = ({ title, children  }: IProps) => {
    return (
        <div className="seo-module">
            <div className="container">
                <div className="row">
                    <div className="col-xs-12" >
                        <div className="seo-module__cardswrapper">
                            {React.Children.map(children, (child, index) => {
                                return (
                                    <div className="seo-module__item" key={`seo-card-${index}`}>
                                        {child}
                                    </div>
                                )
                            })}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    )
}

export default SeoModule;