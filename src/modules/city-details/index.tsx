// Libraries
import * as React from 'react';

// Style
import './style.styl'

// Components
import { Carousel, Button, Tag, Autocomplete, IAutocompleteProps } from '../../components';

interface IProps {
    name?: any
    breadCrumb?: any
    description?: any
    children?: any
}

const CityDetails = ({ name, breadCrumb, description, children }: IProps) => {
    return (
        <div className="citydetails-module">
            <div className="container">
                <div className="row">
                	<div className="col-xs-12" >
	                	<div className="city-name">{name}</div>
	                    <div className="bread-crum">{breadCrumb}</div>
	                    <div className="description">{description}</div>
	                    <div className="heading2"> Popular Districts of Berlin </div>
                        <div className="citydetails-module__cardswrapper">
                            {React.Children.map(children, (child, index) => {
                                return (
                                    <div className="citydetails-module__item" key={`city-card-${index}`}>
                                        {child}
                                    </div>
                                )
                            })}
                        </div>
	                </div>

                </div>
            </div>
        </div>
    )
}

export default CityDetails;